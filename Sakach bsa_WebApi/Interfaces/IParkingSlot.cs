﻿using System;

namespace Sakach_bsa_WebApi.Interfaces
{
    public interface IParkingSlot
    {
        IWallet Wallet { get; }

        IVehicle Vehicle { get; }

        uint UnpaidCounter { get; }

        bool IsVehicleBlocked { get; }

        bool ChargeCost(out double outcome);
    }
}
