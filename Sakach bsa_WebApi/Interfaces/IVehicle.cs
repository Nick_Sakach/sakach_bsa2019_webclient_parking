﻿using System;
using static Sakach_bsa_WebApi.Configuration;

namespace Sakach_bsa_WebApi.Interfaces
{
    public interface IVehicle
    {
        int ID { get; }

        Guid Guid { get; }

        string Name { get; set; }

        VehicleTypes Type { get; }
    }
}
