﻿using System;

namespace Sakach_bsa_WebApi.Interfaces
{
    public interface ITransaction
    {
        DateTime Time { get; }

        string Description { get; }

        string Source { get; }
    }
}
