﻿using System;

namespace Sakach_bsa_WebApi.Interfaces
{
    public interface IWallet
    {
        int ID { get; }

        double Amount { get; }

        Guid Guid { get; }

        void AddMoney(double CostAmount);

        bool TryChargeMoney(double CostAmount);
    }
}
