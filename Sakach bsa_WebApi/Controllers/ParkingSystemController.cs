﻿using System;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using Sakach_bsa_WebApi.Services;
using System.Globalization;
using System.Collections.Generic;

namespace Sakach_bsa_WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingSystemController : ControllerBase
    {
        ParkingSystem parking = ParkingSystem.GetParkingInstance();

        [HttpGet("test")]
        public ActionResult<string> Get()
        {
            return "Connection with Parking Management System established!";
        }

        [HttpGet("showcostamount")]
        public ActionResult<string> GetParkingCostAmount()
        {
            return JsonConvert.SerializeObject(parking.ShowAmount());
        }

        [HttpGet("showslotamount")]
        public ActionResult<string> GetParkingSlotsAmount(DateTime timeFrom)
        {
            return JsonConvert.SerializeObject(parking.ShowSlotStateCounts());
        }

        [HttpGet("showincomes")]
        public ActionResult<string> GetIncomesAmount(string timefrom)
        {
            try
            {
                return JsonConvert.SerializeObject(parking.ShowIncomeSum(DateTime.ParseExact(timefrom, "yyyyMMddHHmmss", CultureInfo.InvariantCulture)));
            }
            catch
            {
                return JsonConvert.SerializeObject(new List<string>() { "An error has occured! Please try again or check DateTime format passed to API!" });
            }
        }
        //addvehicle
        [HttpGet("showslotdetails")]
        public ActionResult<string> GetParkingSlotsDetails()
        {
            return JsonConvert.SerializeObject(parking.ShowParkingSlotsDetails());
        }

        [HttpGet("addvehicle")]
        public ActionResult<string> AddVehicle(string type)
        {
            return JsonConvert.SerializeObject(parking.AddVehicle(type));
        }

        [HttpGet("getwalletinfo")]
        public ActionResult<string> GetWalletInfo(int ID)
        {
            return JsonConvert.SerializeObject(parking.GetWalletInfo(ID));
        }

        [HttpGet("addcosts")]
        public ActionResult<string> AddCosts(int ID, int amount)
        {
            return JsonConvert.SerializeObject(parking.IncreaseBalanceAmount(ID, amount));
        }

        [HttpDelete("removevehicle")]
        public ActionResult<string> Delete(int id)
        {
            return JsonConvert.SerializeObject(parking.RemoveVehicle(id));
        }

        [HttpGet("readtransactions")]
        public ActionResult<string> GetTransactions(string timefrom)
        {
            try
            {
                var logger = TransactionLogger.GetLoggerInstance();
                return JsonConvert.SerializeObject(logger.ReadTransactions(DateTime.ParseExact(timefrom, "yyyyMMddHHmmss", CultureInfo.InvariantCulture)));
            }
            catch
            {
                return JsonConvert.SerializeObject(new List<string>() { "An error has occured! Please try again or check DateTime format passed to API!" });
            }
        }

        [HttpGet("readlog")]
        public ActionResult<string> GetLogHistory()
        {
            try
            {
                var logger = TransactionLogger.GetLoggerInstance();
                return JsonConvert.SerializeObject(logger.ReadLog());
            }
            catch
            {
                return JsonConvert.SerializeObject(new List<string>() { "An error has occured! Please try again or check DateTime format passed to API!" });
            }
        }
    }
}