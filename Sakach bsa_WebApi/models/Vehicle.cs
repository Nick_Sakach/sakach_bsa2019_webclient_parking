﻿using System;
using Sakach_bsa_WebApi.Interfaces;
using static Sakach_bsa_WebApi.Configuration;

namespace Sakach_bsa_WebApi.models
{
    public class Vehicle : IVehicle
    {
        private static int idCounter = 1;

        public int ID { get; }

        public Guid Guid { get; private set; }

        public string Name { get; set; }

        public VehicleTypes Type { get; private set; }

        public Vehicle(VehicleTypes type)
        {
            this.ID = idCounter++;
            this.Guid = Guid.NewGuid();
            this.Name = "";
            this.Type = type;
        }
    }
}
