﻿using Sakach_bsa_WebApi.Interfaces;
using System;

namespace Sakach_bsa_WebApi.models
{
    public class Transaction : ITransaction
    {
        public DateTime Time { get; }

        public string Description { get; }

        public string Source { get; }

        public override string ToString()
            => this.Time + " -> " + this.Source + ": " + this.Description;

        public Transaction(string tranSource, string tranDescr)
        {
            this.Time = DateTime.Now;
            this.Source = tranSource;
            this.Description = tranDescr;
        }
    }
}
