﻿using Sakach_bsa_WebApi.Interfaces;
using System;

namespace Sakach_bsa_WebApi.models
{
    public class BillingAccount : IWallet
    {
        private static int idCounter = 1;

        public int ID { get; }

        public Guid Guid { get; private set; }

        public double Amount { get; private set; }

        public void AddMoney(double CostAmount)
        {
            try
            {
                Amount += CostAmount;
            }
            catch (OverflowException)
            {
                Console.WriteLine("Budget inceasement overflow exception! Please check budget's maximum size (doble's max value )");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Adding costs to budget error: " + ex.Message);
            }
        }

        public bool TryChargeMoney(double CostAmount)
        {
            bool result = false;
            try
            {
                if (CostAmount > Amount) return false;

                Amount -= CostAmount;
                result = true;
            }
            catch (OverflowException)
            {
                Console.WriteLine("Budget substraction overflow exception! Please check budget's size limits (doble's max/min values )");
            }
            catch (Exception ex)
            {
                Console.WriteLine("TryChargeMoney from budget error: " + ex.Message);
            }

            return result;
        }

        public BillingAccount(double initAmount)
        {
            this.ID = idCounter++;
            this.Amount = initAmount;
            this.Guid = Guid.NewGuid();
        }
    }
}
