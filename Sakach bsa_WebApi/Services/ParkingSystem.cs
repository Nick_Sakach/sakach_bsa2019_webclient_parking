﻿using Sakach_bsa_WebApi.Interfaces;
using Sakach_bsa_WebApi.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Sakach_bsa_WebApi.Configuration;

namespace Sakach_bsa_WebApi.Services
{
    public class ParkingSystem
    {
        private static ParkingSystem parking;
        private static TransactionLogger Logger = TransactionLogger.GetLoggerInstance();

        public Dictionary<DateTime, double> Incomes = new Dictionary<DateTime, double>();

        public IWallet Budget { get; private set; }

        public IParkingSlot[] Slots { get; set; }
        private ParkingSystem()
        {
            Slots = new ParkingSlot[ParkingSlotsCont];
            Slots[0] = new ParkingSlot(VehicleTypes.Truck, 25000);
            Budget = new BillingAccount(0);
        }

        public static ParkingSystem GetParkingInstance()
        {
            if (parking == null)
            {
                parking = new ParkingSystem();
            }
            return parking;
        }

        public string ShowAmount()
        {
            return $"Current Parking's balance is {parking.Budget.Amount} {Configuration.СurrencyName}";
        }

        public string ShowIncomeSum(DateTime timeFrom)
        {
            var amount = Incomes.Where(x => x.Key > timeFrom).Select(x => x.Value).Sum();
            return $"For mentioned period cost income was {amount} {Configuration.СurrencyName}";
        }

        public List<string> ShowSlotStateCounts()
        {
            var totalCount = Slots.Length;
            var free = Slots.Where(x => x == null).Count();

            List<string> result = new List<string>()
            {
                $"Total Parking slots: {totalCount}",
                $"Free Parking slots: {free}",
                $"Used Parking slots: {totalCount - free}"
            };
            return result;
        }

        public List<string> ShowParkingSlotsDetails()
        {
            List<string> details = new List<string>();
            for (int i = 0; i < Slots.Length; i++)
            {
                details.Add($"Slot {i} info:");
                var slotInfo = Slots[i] == null ? " Empty." : GetSlotInfo(Slots[i]);
                details.Add(slotInfo);
                details.Add(String.Empty);
            }
            return details;
        }

        public string GetSlotInfo(IParkingSlot slot)
        {
            StringBuilder result = new StringBuilder();
            result.AppendLine();
            result.AppendLine("Car info:");
            result.AppendLine($"Type: {slot.Vehicle.Type.ToString()}");
            result.AppendLine($"ID: {slot.Vehicle.ID}");
            result.AppendLine($"GUID: {slot.Vehicle.Guid}");
            result.AppendLine($"Payer wallet's ID: {slot.Wallet.ID}");
            result.AppendLine($"Payer wallet's GUID: {slot.Wallet.Guid}");
            result.AppendLine($"Payer's amount: {slot.Wallet.Amount}");
            result.AppendLine($"Is car blocked: {slot.IsVehicleBlocked}");
            if (slot.UnpaidCounter > 0)
                result.AppendLine($"Times costs weren't paid: {slot.UnpaidCounter}");
            return result.ToString();
        }

        public string AddVehicle(string input)
        {
            string result = String.Empty;
            if (Slots.Where(x => x == null).Any() == false)
            {
                result = "All parking lots are already used!";
                return result;
            }

            VehicleTypes type;
            switch (input)
            {
                case "1": type = VehicleTypes.Passenger; break;
                case "2": type = VehicleTypes.Bike; break;
                case "3": type = VehicleTypes.Bus; break;
                case "4": type = VehicleTypes.Truck; break;
                default: type = VehicleTypes.Passenger; break;
            }

            IParkingSlot slot;
            slot = new ParkingSlot(type);
            for (int i = 0; i < Slots.Length; i++)
            {
                if (Slots[i] == null)
                {
                    Slots[i] = slot;
                    break;
                }
            }
            result = $"Vehicle {slot.Vehicle.Guid} (ID:{slot.Vehicle.ID}, {slot.Vehicle.Type.ToString()}) with amount {slot.Wallet.Amount}{СurrencyName} created!";
            return result;
        }

        public string RemoveVehicle(int ID)
        {
            string result = String.Empty;
            var slot = Slots.Where(x => x?.Vehicle?.ID == ID).FirstOrDefault();
            if (slot == null)
            {
                result = "Vehicle with such GUID is not found. Check your input";
            }
            else
            {
                if (slot.IsVehicleBlocked)
                {
                    result = "Vehicle with such ID is currently blocked! Please increase corresponding wallet's amount to pay taxes and unblock it."
                        + "(This time you can just add any amount of money you want for free, but next time be more attentive to your balance)";
                }
                else
                {
                    for (int i = 0; i < Slots.Length; i++)
                    {
                        if (Slots[i].Vehicle.ID == ID)
                        {
                            Slots[i] = null;
                            break;
                        }
                    }
                    result = "Done.";
                }
            }
            return result;
        }

        public string GetWalletInfo(int ID)
        {
            string result = String.Empty;

            var walletSlot = Slots.Where(x => x?.Wallet?.ID == ID).FirstOrDefault();
            if(walletSlot!= null)
            {
                result = $"Wallet {walletSlot.Wallet.Guid.ToString()} (ID: {walletSlot.Wallet.ID}) "
                    + $"of vehicle {walletSlot.Vehicle.Guid.ToString()} (ID: {walletSlot.Vehicle.ID}, {walletSlot.Vehicle.Type})"
                    + $", amount: {walletSlot.Wallet.Amount}";
            }
            return result;
        }

        public string IncreaseBalanceAmount(int ID, int sum)
        {
            var result = String.Empty;
            var wallet = Slots.Where(x => x?.Wallet?.ID == ID).Select(x => x?.Wallet).FirstOrDefault();

            if (wallet == null)
            {
                result = "Wallet with such ID wasn't found!";
            }
            else
            {
                if (sum > 0)
                {
                    wallet.AddMoney(sum);
                    result = "Done.";
                }
                else
                {
                    result = "Incorrect value! Please check that it's positive!";
                }
            }

            return result;
        }

        public void CollectCosts(object _)
        {
            bool isCostCharged;
            foreach (var item in Slots)
            {
                try
                {
                    if (item != null)
                    {
                        isCostCharged = item.ChargeCost(out double outcome);

                        var details = isCostCharged ? $"Rent {outcome}{Configuration.СurrencyName} was paid successfully."
                            : $"Rent wasn't paid {item.UnpaidCounter} time(s). Car is currently blocked";

                        var transaction = new Transaction($"Vehicle {item.Vehicle.Guid} ({item.Vehicle.Type.ToString()})", details);
                        Logger.WriteToLog(transaction);

                        if (outcome > 0)
                        {
                            parking.Budget.AddMoney(outcome);
                            Incomes.Add(DateTime.Now, outcome);
                            transaction = new Transaction("Parking", $"Amount {outcome}{Configuration.СurrencyName} was transferred to budget.");
                            Logger.WriteToLog(transaction);
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Collect Costs error: " + ex.Message);
                }
            }
        }
    }
}
