﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Sakach_bsa_WebApi.Interfaces;

namespace Sakach_bsa_WebApi.Services
{
    public class TransactionLogger
    {
        private static readonly string LogPath = $"..\\TransactionLog {DateTime.Now.ToString("yyyy_MM_dd")}.txt";
        private static Lazy<TransactionLogger> logger = new Lazy<TransactionLogger>(()=> new TransactionLogger());

        public static Dictionary<DateTime, string> Transactions = new Dictionary<DateTime, string>();

        private TransactionLogger() { }

        public static TransactionLogger GetLoggerInstance() => logger.Value;

        public void WriteToLog(ITransaction transaction)
        {
            try
            {
                Transactions.Add(DateTime.Now, transaction.ToString());
                using (FileStream fstream = new FileStream(LogPath, FileMode.OpenOrCreate))
                {
                    byte[] logText = Encoding.Default.GetBytes(transaction.ToString() + "\n\n");

                    fstream.Seek(0, SeekOrigin.End);
                    fstream.Write(logText);

                }
            }
            catch (UnauthorizedAccessException)
            {
                Console.WriteLine("\n" + "Logging error: File is not accesible! Please check permissions or run this program as Admin.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("\n" + "WriteToLog error: " + ex.Message);
            }
        }

        public List<string> ReadLog()
        {
            List<string> details = new List<string>();
            try
            {
                using (FileStream fstream = new FileStream(LogPath, FileMode.OpenOrCreate))
                {
                    fstream.Seek(0, SeekOrigin.Begin);
                    var output = new byte[fstream.Length];

                    fstream.Read(output, 0, output.Length);
                    details.AddRange(("Transation Log file info: \n\n" + Encoding.Default.GetString(output)).Split("\n"));
                }
            }
            catch (UnauthorizedAccessException)
            {
                Console.WriteLine("\n" + "Logging error: File is not accesible! Please check permissions or run this program as Admin.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("\n" + "WriteToLog error: " + ex.Message);
            }
            return details;
        }

        public List<string> ReadTransactions(DateTime timeFrom)
        {
            List<string> details = new List<string>();

            details.Add("Transaction list for specified amount of time:");
            details.AddRange(Transactions.Where(x => x.Key > timeFrom).Select(x => x.Value).ToList());

            return details;
        }
    }
}
