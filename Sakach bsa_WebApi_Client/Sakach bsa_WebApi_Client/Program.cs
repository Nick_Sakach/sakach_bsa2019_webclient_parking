﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Sakach_bsa_WebApi_Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Parking Management System client! Awaiting for server's responce... \n\n");

            var serverAccessible = Task.Run(() => HttpRequester.CheckConnection()).Result;
            if (serverAccessible)
            {
                var continueFlow = true;
                while (continueFlow)
                {
                    continueFlow = Menues.MainMenu();
                }
                Console.Write("Thanks for using our sysytem!");
            }

            Console.WriteLine("Press any key to quit...");
            Console.ReadLine();
        }
    }
}
