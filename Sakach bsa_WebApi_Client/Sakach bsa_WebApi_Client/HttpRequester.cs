﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Sakach_bsa_WebApi_Client
{
    class HttpRequester
    {
        public static string ConnectionStringBase { get; private set; } = "https://localhost:9989/api/parkingsystem";

        public static bool CheckConnection()
        {
            var result = false;

            string data = SendGetRequest("/test");
            if (data != String.Empty)
            {
                Console.WriteLine(data);
                result = true;
            }
            return result;
        }

        public static void ShowAmount()
        {
            string data = SendGetRequest("/showcostamount");
            if (data != String.Empty)
            {
                Console.WriteLine(JsonConvert.DeserializeObject<string>(data));
            }            
        }

        public static void ShowSlotAmount()
        {
            string data = SendGetRequest("/showslotamount");
            if (data != String.Empty)
            {
                var responce = JsonConvert.DeserializeObject<List<string>>(data);
                responce.ForEach(x => Console.WriteLine(x));
                Console.WriteLine();
            }
        }

        public static void ShowIncomeSum(DateTime timeFrom)
        {
            string data = SendGetRequest("/showincomes?timefrom=" + timeFrom.ToString("yyyyMMddHHmmss"));
            if (data != String.Empty)
            {
                Console.WriteLine(JsonConvert.DeserializeObject<string>(data));
            }
        }

        public static void ShowParkingSlotsDetails()
        {
            string data = SendGetRequest("/showslotdetails");
            if (data != String.Empty)
            {
                var responce = JsonConvert.DeserializeObject<List<string>>(data);
                responce.ForEach(x => Console.WriteLine(x));
                Console.WriteLine();
            }
        }

        public static void AddVehicle()
        {
            Console.WriteLine("4. Add Vehicle \n");
            Console.WriteLine("Select creatable's car type:");
            Console.WriteLine("1. Passenger");
            Console.WriteLine("2. Bike");
            Console.WriteLine("3. Bus");
            Console.WriteLine("4. Truck");
            var input = Console.ReadLine();

            string data = SendGetRequest("/addvehicle?type=" + input);
            if (data != String.Empty)
            {
                Console.WriteLine(JsonConvert.DeserializeObject<string>(data));
            }
        }

        public static void IncreaseBalanceAmount()
        {
            Console.WriteLine("6. Add some amount of money to specific wallet \n");
            Console.WriteLine("Please input wallet's ID below: ");
            Int32.TryParse(Console.ReadLine(), out int id);

            string data = SendGetRequest("/getwalletinfo?id=" + id);
            if (JsonConvert.DeserializeObject<string>(data) != String.Empty)
            {
                Console.WriteLine(JsonConvert.DeserializeObject<string>(data) + "\n");
                Console.WriteLine("Input amount to add below:");
                Int32.TryParse(Console.ReadLine(), out int amount);
                data = SendGetRequest($"/addcosts?id={id}&amount={amount}");
                if (data != String.Empty)
                {
                    Console.WriteLine(JsonConvert.DeserializeObject<string>(data));
                }
            }
            else
            {
                Console.WriteLine("Such wallet wasn't found.");
            }
        }

        public static void RemoveVehicle()
        {
            Console.WriteLine("5. Remove Vehicle \n");
            Console.WriteLine("Please input car's ID below: ");
            Int32.TryParse(Console.ReadLine(), out int id);
            string data = SendDeleteRequest("/removevehicle?id=" + id);
            if (data != String.Empty)
            {
                Console.WriteLine(JsonConvert.DeserializeObject<string>(data));
            }
        }

        public static void ReadTransactions(DateTime timefrom)
        {
            string data = SendGetRequest("/readtransactions?timefrom=" + timefrom.ToString("yyyyMMddHHmmss"));
            if (data != String.Empty)
            {
                var responce = JsonConvert.DeserializeObject<List<string>>(data);
                responce.ForEach(x => Console.WriteLine(x + "\n"));
                Console.WriteLine();
            }
        }

        public static void ReadLog()
        {
            string data = SendGetRequest("/readlog");
            if (data != String.Empty)
            {
                var responce = JsonConvert.DeserializeObject<List<string>>(data);
                responce.ForEach(x => Console.WriteLine(x));
                Console.WriteLine();
            }
        }

        public static string SendGetRequest(string uri)
        {
            string reqestUri = ConnectionStringBase + uri;
            string result = String.Empty;

            try
            {
                using (HttpClient client = new HttpClient())
                using (HttpResponseMessage responce = Task.Run(() => client.GetAsync(reqestUri)).Result)
                using (HttpContent content = responce.Content)
                {
                    if (!responce.IsSuccessStatusCode)
                        throw new HttpRequestException();
                    string data = Task.Run(() => content.ReadAsStringAsync()).Result;
                    result = data ?? String.Empty;
                }

            }
            catch (HttpRequestException)
            {
                //TODO: Output URI with error
                Console.WriteLine($"Http Request exception occured! Please check your connection and app's URI accesibility: ({reqestUri})");
            }
            catch (Exception ex)
            {
                Console.WriteLine("OOPS! Exception! \n\n" + ex.Message);
            }

            return result;
        }

        public static string SendDeleteRequest(string uri)
        {
            string reqestUri = ConnectionStringBase + uri;
            string result = String.Empty;

            try
            {
                using (HttpClient client = new HttpClient())
                using (HttpResponseMessage responce = Task.Run(() => client.DeleteAsync(reqestUri)).Result)
                using (HttpContent content = responce.Content)
                {
                    if (!responce.IsSuccessStatusCode)
                        throw new HttpRequestException();
                    string data = Task.Run(() => content.ReadAsStringAsync()).Result;
                    result = data ?? String.Empty;
                }

            }
            catch (HttpRequestException)
            {
                //TODO: Output URI with error
                Console.WriteLine($"Http Request exception occured! Please check your connection and app's URI accesibility: ({reqestUri})");
            }
            catch (Exception ex)
            {
                Console.WriteLine("OOPS! Exception! \n\n" + ex.Message);
            }

            return result;
        }
    }
}
