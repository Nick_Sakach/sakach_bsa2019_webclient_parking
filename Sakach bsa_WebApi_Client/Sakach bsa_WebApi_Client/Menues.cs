﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sakach_bsa_WebApi_Client
{
    class Menues
    {
        public static bool MainMenu()
        {
            var continueFlow = true;

            Console.WriteLine();
            Console.WriteLine("#####  MAIN MENU  #####");
            Console.WriteLine();
            Console.WriteLine("Select SubMenu (input its number/letter and press ENTER):");
            Console.WriteLine("0. Show system`s amount (balance)");
            Console.WriteLine("1. Show system`s income in last minute period");
            Console.WriteLine("2. Output current count of free/used places");
            Console.WriteLine("3. Output current parking slots info");
            Console.WriteLine("4. Add Vehicle");
            Console.WriteLine("5. Remove Vehicle");
            Console.WriteLine("6. Add some amount of money to specific wallet");
            Console.WriteLine("7. Get transactions for last minute");
            Console.WriteLine("8. Get transaction info from TransactionLog.txt");
            Console.WriteLine("C. Edit configuration");
            Console.WriteLine("H. Help Menu (some basic info)");
            Console.WriteLine("Q. Quit");
            Console.WriteLine();
            var variant = Console.ReadLine();
            Console.Clear();

            switch (variant.ToLower())
            {
                case "0": HttpRequester.ShowAmount(); break;
                case "1": HttpRequester.ShowIncomeSum((DateTime.Now.AddMinutes(-1))); break;
                case "2": HttpRequester.ShowSlotAmount(); break;
                case "3": HttpRequester.ShowParkingSlotsDetails(); break;
                case "4": HttpRequester.AddVehicle(); break;
                case "5": HttpRequester.RemoveVehicle(); break;
                case "6": HttpRequester.IncreaseBalanceAmount(); break;
                case "7": HttpRequester.ReadTransactions(DateTime.Now.AddMinutes(-1)); break;
                case "8": HttpRequester.ReadLog(); break;
                case "c": ComingSoon(); break;
                case "h": ComingSoon(); break;
                case "q": return false;
                default: UnrecognizedInput(); break;

            }

            return continueFlow;
        }

        static void UnrecognizedInput() => Console.WriteLine("\n" +
            "We haven't matched your input with current functionality. \n"
            + "Please check your input and try again. \n");

        static void ComingSoon() =>
            Console.WriteLine("\n" + "Coming soon...\n");
    }
}
